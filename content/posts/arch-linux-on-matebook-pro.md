---
title: Installing Arch Linux on Huawei Matebook X Pro
date: 2021-12-05
published-on: 2021-12-05
authors:
    - Joni Korhonen
keywords:
    - arch linux
    - laptop
    - install
    - huawei matebook x pro 2018
    - full-disk encryption
description: Installing Arch Linux on Huawei Matebook X Pro 2018 model with full-disk encryption
---

After some not so successful experimentation my laptop was left in quite a battered condition install wise and anyway I should have enabled full-disk encryption when I first installed operating system on this computer, so here we go, from scratch install of Arch Linux.

In this post we install [Arch Linux](https://archlinux.org) on Huawei Matebook X Pro 2018 model, which comes with an i7 8th Gen. Intel CPU, 16Gib of RAM, 512Gib of SSD storage, Nvidia MX150 GPU with 2Gib of DRAM, and a HiDPI 3000x2000 touchscreen. I'm going to setup full-disk encryption and configure user land based on my [dotfiles](https://gitlab.com/jonikorhonen/DotfilesRedux) and go through some pain with reinstalling system as I had forgotten some configurations that make using this laptop better experience.

*Disclaimer:* This is not in any way official install documentation for Arch Linux and it is highly suggested that you follow the official [Arch Linux Installation Guide](https://wiki.archlinux.org/title/Installation_guide), or better yet, reconsider what ever choices you made in life that made you want to install Arch. This post is mainly just notes for myself and details may change at any time.

## Preparing for install

Now all that is out of the way, lets start with fetching latest install image and making a bootable USB stick with it. Go to [Arch download page](https://archlinux.org/download/) and get latest x86-64 iso image and PGP signature (Note: If you downloaded image from mirror, download GPG signature from main site to lessen change of manipulated image and signature combination). And then verify downloaded image (make sure image and signature are in same directory):

```
$ pacman-key -v archlinux-VERSION-x86-64.iso.sig
```

If all is good, continue with finding out which device is your USB stick with eg. `dmesg` (we'll assume it was /dev/sdb) and making install USB with `dd`

```
$ sudo dd if=archlinux-VERSION-x86-64.iso of=/dev/sdb bs=1M status=progress
```

When that is done lets continue with booting to install environment.

## Booting from USB

Secure boot will most likely mess with installation so turn it off from BIOS settings. On this computer it is done with holding down F2 while booting. Once in BIOS disable secure boot and any devices you're not going to need, in my case I disabled camera, bluetooth, and fingerprint sensor, then Save and Exit. To enter boot menu hold down F12 and select `EFI USB device`.

## Installing Arch Linux

Large parts of this section follow official install guide.

Default keymap doesn't fly with me, so changed it to Finnish

```
# loadkeys fi
```

Make sure we are in EFI mode with

```
# ls /sys/firmware/efi
```

if that directory exists, all is good

### Internet connection

As this computer doesn't have ethernet ports and I don't own dongle with one we need to get wireless up. Ensure that wireless interface is found and enabled with

```
# ip link
```

and then authenticated to wifi network with [iwctl](https://wiki.archlinux.org/title/Iwd#iwctl).

Check that connection is working

```
# ping -c 3 archlinux.org
```

and update system time

```
# timedatectl set-ntp true
```

### Disk partitions

First wipe the disk to destroy any existing data and partitions. We'll follow [dm-crypt disk wipe instruction](https://wiki.archlinux.org/index.php/Dm-crypt/Drive_preparation#dm-crypt_wipe_on_an_empty_disk_or_partition)

Find out what is your hard drive device

```
# lsblk
```

I have PCIE ssd, so it is recognized as `/dev/nvme0n1`, with partitions as `p1`, `p2`, etc suffix.

```
# cryptsetup open --type plain -d /dev/urandom /dev/nvme0n1 to_be_wiped
```

Verify that the new device exists

```
# lsblk
```

Now write to the device with zeros, which turns into apparent randomness on disk as it is a encrypted device

```
# dd if=/dev/zero of=/dev/mapper/to_be_wiped bs=1M status=progress
```

This will take some time, in my case around 20 minutes, so go grap a drink and some snacks.

After that close the temporary container

```
# cryptsetup close to_be_wiped
```

Next I partitioned and created a 512MB fat 32 partition of type `EFI type` and rest of the 512 GB disk was partitioned as `Linux filesystem` for Arch. I used `cfdisk`

Format the EFI/boot partition

```
# mkfs.fat -F32 /dev/nvme0n1p1
```

### Full-disk encryption and partitions

Disk encryption is done as a LVM on LUKS with base instructions from article [Encrypting an entire system](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS) in Arch wiki. Setup will be a single LUKS2 volume with LVM on top which divides that volume to root, home, and swap partitions.

Setup and open LUKS2 volume. These steps prompt for password, don't forget what you set it as!

```
# cryptsetup luksFormat --type luks2 /dev/nvme0n1p2
# cryptsetup open /dev/nvme0n1p2 cryptlvm
```

Next setup LVM volumes, with swap at least as large as your RAM to support hibernate

```
# pvcreate /dev/mapper/cryptlvm
# vgcreate archvg /dev/mapper/cryptlvm
# lvcreate -L 16G archvg -n swap
# lvcreate -L 128G archvg -n root
# lvcreate -l 100%FREE archvg -n home
```

Format the filesystems

```
# mkfs.ext4 /dev/archvg/root
# mkfs.ext4 /dev/archvg/home
# mkswap /dev/archvg/swap
```

Mount encrypted partitions and enable swap

```
# mount /dev/archvg/root /mnt
# mkdir /mnt/home
# mount /dev/archvg/home /mnt/home
# swapon /dev/archvg/swap
```

Mount Boot/EFI volume

```
# mkdir /mnt/boot
# mount /dev/nvme0n1p1 /mnt/boot
```

### Basic configuration for system

This mostly comes from official [Installation Guide](https://wiki.archlinux.org/title/Installation_guide).

Install base system with necessary components to have everything needed to get internet connection once we boot to installed system, adjust yours with what you want to have in bare bones system.

```
# pacstrap /mnt base base-devel intel-ucode linux linux-firmware lvm2 dialog wpa_supplicant wifi-menu netctl dhcpcd vim man-db man-pages
```

Generate fstab (`-U` to use UUID labels)

```
# genfstab -U /mnt >> /mnt/etc/fstab
```

and validate that it looks correct. You may want to set `noatime` for partitions, heres what mine ended up looking like afterwards:

```
# Static information about the filesystems.
# See fstab(5) for details.

# <file system> <dir> <type> <options> <dump> <pass>
# /dev/mapper/archvg-root
UUID=8e80acf0-c8c7-4713-800f-df08ef854913       /               ext4            rw,relatime,noatime,discard     0 1

# /dev/mapper/archvg-home
UUID=a16da72e-faec-424a-8ffe-f05e7fd7edd0       /home           ext4            rw,relatime,noatime,discard     0 2

# /dev/nvme0n1p1
UUID=0832-A46C          /boot           vfat            rw,noatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro    0 2

# /dev/mapper/archvg-swap
UUID=e9f803e2-a562-4d73-ab98-a7128c5ce30a       none            swap            defaults        0 0
```

Next chroot in to the new system

```
# arch-chroot /mnt
```

Now that we are in the new system, it is time for some more configuring.

Set the timezone and run hwclock

```
# ln -sf /usr/share/zoneinfo/Europe/Helsinki /etc/localtime
# hwclock --systohc
```

Edit `/etc/locale.gen` and uncommnet needed locales, `fi_FI.UTF-8` and `en_US.UTF-8` for me. Then create `/etc/locale.conf` to set `LANG` variables.

```
LANG=fi_FI.UTF-8
LC_MESSAGES=en_US.UTF-8
```

Set the console keyboard layout to persist by editing `/etc/vconsole.conf`

```
KEYMAP=fi
```

Set hostname

```
# echo "lappy" > /etc/hostname
```

Set root password

```
# passwd
```

Edit `/etc/mkinitcpio.conf` to enable encryption and booting to lvm volume, by editing the `HOOKS` line to include `encrypt` and `lvm`, position matters

```
...
HOOKS=(base udev autodetect modconf block encrypt lvm2 resume filesystems keyboard fsck)
...
```

then generate initramfs

```
# mkinitcpio -p linux
```

### Bootloader

I'm using systemd-boot as my bootloader, check [The Good Wiki](https://wiki.archlinux.org/title/Arch_boot_process#Boot_loader) for other options.

First create loader with `bootctl`

```
# bootctl --path=/boot/ install
```

Create loader configuration for Arch in `/boot/loader/entries/arch.conf`. Mine ended up looking like this

```
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options cryptdevice=UUID=de4fb835-19f4-48c8-9ebd-327e60b8f764:cryptlvm root=/dev/mapper/archvg-root quiet rw
```

You can get device id for crypted harddrive with `blkid`. Look for entry with something like `TYPE="crypto_LUKS"`. If you're vim user as is proper, you can read output from blkid directly to vim with `:r !blkid`. Remember that UUID value must be without quotation marks. Root is lvm container for root partition we created earlier.

Edit `/boot/loader/loader.conf` to include loader configuration for Arch:

```
timeout 3
editor 0
default arch
```

`editor 0` is highly recommended to disable kernel parameter editing for better security.

Time to reboot and cross fingers that everything went as it should.

## First boot

You should see boot loader screen which will automatically boot into Arch after few seconds. Following that there should come prompt for encryption password, enter that and press enter.

In the login prompt login as `root` with its password. Now that we are in, connect to wifi with `wifi-menu` and with that next in line is adding our normal user and getting everything installed and ready for normal use.

```
# useradd -m -G wheel -s /bin/bash <USER>
# passwd <USER>
```

Make our new users sudo powers reality with running `visudo` and uncommenting following line

```
## Uncomment to allow members of group wheel to execute any command
#%wheel ALL=(ALL) ALL # <-- This line
```

### Dash as /bin/sh and other pacman hooks

I like to have dash as `/bin/sh` so add a pacman hook for it to `/etc/pacman.d/hooks/101-dash-sh.hook`

```
[Trigger]
Type = Package
Operation = Install
Operation = Upgrade
Target = bash

[Action]
Description = Re-pointing /bin/sh symlink to dash...
When = PostTransaction
Exec = /usr/bin/ln -sfT dash /usr/bin/sh
Depends = dash
```

install dash and reinstall bash to run the hook

```
# pacman -S dash bash
```

If you use systemd-boot you'll need a post-install hook for it to run `bootctl update` after systemd upgrades, add it to `/etc/pacman.d/hooks/100-systemd-boot.hook`

```
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot...
When = PostTransaction
Exec = /usr/bin/bootctl update
```

### Xorg, display drivers, and touchpad

Some day I'm going to go with Wayland, but today is not the day, so Xorg it is. This laptop has both intel and nvidia GPUs so account for that.

```
# pacman -S xorg-server xorg-fonts-100dpi xorg-fonts-75dpi xorg-font-util xorg-mkfontscale \
    xorg-xbacklight xorg-xdpyinfo xorg-xmodmap xorg-xrandr xorg-xrdb xorg-xset xorg-xwininfo \
    xf86-input-synaptics xf86-video-intel nvidia mesa bumblebee bbswitch acpi_call
```

Enable Bumblebee with bbswitch for nvidia / intel switching

```
# systemctl enable bumblebeed.service
# usermod -a -G <USER> bumblebee
```

Configure intel driver to allow display backlight adjustments by adding configuration file to `/ect/X11/xorg.conf.d/20-intel.conf`

```
Section "Device"
        Identifier      "Intel Graphics"
        Driver          "intel"
        Option          "Backlight"             "intel_backlight"
EndSection
```

Set X11 keyboard layout with localectl, which saves configuration to `/etc/X11/xorg.conf.d/00-keyboard.conf`

```
# localectl set-x11-keymap fi
```

Configure touchpad in `/etc/X11/xorg.conf.d/30-synaptics.conf`

```
Section "InputClass"
        Identifier      "touchpad"
        Driver          "libinput"
        MatchIsTouchPad "on"
        Option          "Tapping"               "on"
        Option          "TappingButtonMap"      "lmr"
        Option          "NaturalScrolling"      "true"
        Option          "AccelSpeed"            "0.75"
EndSection
```

### Networking

Install and enable NetworkManager

```
# pacman -S networkmanager
# systemctl enable NetworkManager.service
# systemctl start NetworkManager.service
# nmcli device wifi connect '<network>' password <password>
```

### Power management

We'll use [TLP](https://linrunner.de/tlp/) for power management

```
# pacman -S tlp tlp-rdw ethtool lsb-release smartmontools
```

Enable TLP and mask services to avoid conflicts with TLP's radio device switching

```
# systemctl enable tlp.service
# systemctl mask systemd-rfkill.service
# systemctl mask systemd-rfkill.socket
# systemctl enable NetworkManager-dispatcher.service
```

See [TLP Arch wiki page](https://wiki.archlinux.org/title/TLP#Configuration) for additional configuration you might want.

Enable audio power saving in `/etc/modprobe.d/audio-powersave.conf`

```
options snd_hda_intel power_save=1
```

This computer comes with Intel Wireless 8265 so we can use `iwlwifi` power saving options. Add these to `/etc/modprobe.d/iwlwifi.conf`

```
options iwlwifi power_save=1 d0i3_disable=0
options iwldvm force_cam=0
```

For intel GPU power savings, add `/etc/modprobe.d/i915.conf`

```
options i915 enable_guc=3 enable_fbc=1
```

These settings should give comfortable 8 to 10 hours of use, provided it is not heavy.

### Graphical user interface

I'm a i3 user, so let's install it, fonts, and some other packages my configuration uses

```
# pacman -S i3-gaps i3status dmenu tmux lightdm lightdm-gtk-greeter compton dunst maim xsel git \
    alsa-utils pulseaudio pulseaudio-alsa alsa-tools pavucontrol pulsemixer \
    noto-fonts noto-fonts-extra noto-fonts-emoji noto-fonts-cjk adobe-source-code-pro-fonts \
    adobe-source-sans-fonts adobe-source-serif-fonts neovim python3 python-neovim ranger \
    zathura zathura-pdf-mupdf firefox xscreensaver xwallpaper network-manager-applet zsh pandoc jq
```

Enable lightdm

```
# systemctl enable lightdm.service
```

At this point I login as my user, clone my [dotfiles](https://gitlab.com/jonikorhonen/DotfilesRedux), copy over configs in their right places, and install [Oh My Zsh](https://ohmyz.sh/) (I know, bloat).

Reboot and you should be eventually greeted by lightdm login screen.

## Getting audio fully working

My laptop has 4.0 speaker setup, which needed a bit of work to get working correctly, I ended up running `hdajackretask` from root terminal. In there selected Realtek ALC256 from dropdown then Show unconnected pins and Advanced override from side panel. In pin configuration selected override for pins `0x14` and `0x1b`, set connectivity to internal for both, installed boot override, and run `mkinitcpio -P`.

After reboot I could in `pavucontrol` select Analog Surround 4.0 in the configuration tab.

## All done

System should be configured and ready to use now.

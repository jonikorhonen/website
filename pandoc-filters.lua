-- See https://pandoc.org/lua-filters.html

function Meta(m)
    if m.date == nil then
        -- Add date metadata in YYYY-MM-DD format for documents lacking it
        m.date = os.date("%F")
        return m
    end
end

function Link (elem)
    -- TODO: only apply to external links
    -- elem.attributes.rel = 'nofollow'
    return elem
end